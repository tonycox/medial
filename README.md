# Medial

Medial is a web api designed principally for the mediation of a turn-based game (e.g. connect 4) being
played by multiple clients.  Clients send requests to Medlial to get the current state of the system (e.g. the current
board state, whose turn it is, etc.), and send a request to Medial to update the state to a new state that they send through (e.g. clients
'make a move' and send it to Medial).  Medial then validates the new state (e.g. that the move was a legal move that
the client was allowed to make) based on a given validation function and, once validated, updates the current state to
be the new state.

All communication is from clients via http request/response GET and POST requests.  In this way, clients only interact
with Medial and not with each other, thus the system is like a big web-based implementation of the mediator pattern.
All communcated data is via serialised JSON strings.

Medial is at its core a web-based game server, and was originally written to host Connect 4 matches between two AI
clients, bu it is highly extensible and can easily be used for non-game mediation between clients in any architecture
that may require the separation of multiple clients via a web API.ion


## License
TODO

## Contribution
TODO

## API Documentation
TODO