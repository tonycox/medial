from medial.game import base
from medial.store import get_store
from medial.api import application
from medial import exc
import random
import string


def get_game(app: application.App, key: str) -> base.Game:
    # retrieves an existing game
    s = get_store(key)
    game_type_name = s.get(s.CONTEXT_TYPE)
    if game_type_name is None:
        raise exc.MedialKeyError('No game exists with key {}'.format(key))
    if game_type_name not in app.game_types.keys():
        raise exc.MedialKeyError('Unknown game type {}.'.format(game_type_name))
    return app.game_types[game_type_name](key)


def game_exists(key: str) -> bool:
    s = get_store(key)
    if s.get(s.CONTEXT_TYPE) is None:
        return False
    return True


def _generate_new_key() -> str:
    while True:
        candidate_key = ''.join(random.choices(string.ascii_uppercase, k=4))
        if not game_exists(candidate_key):
            return candidate_key


def create_new_game(app, game_type_name: str, key: str=None) -> base.Game:
    if key is not None and game_exists(key):
        raise exc.MedialAssertionError('A game with key {} already exists!'.format(key))
    key = key or _generate_new_key()
    # check that we know how to run the given game
    if game_type_name not in app.game_types.keys():
        raise exc.MedialAssertionError("Unknown game type: ".format(game_type_name))
    return app.game_types[game_type_name](key)
