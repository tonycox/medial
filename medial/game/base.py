from abc import ABC
import enum
import inflection

from medial import store, exc


from typing import List


class ValidationException(Exception):
    pass


class CompletionState(enum.Enum):
    COMPLETE = 'Complete'
    INCOMPLETE = 'Incomplete'

    @classmethod
    def from_value(cls, value):
        result = next((c for c in cls if c.value == value), None)
        if result is None:
            raise exc.MedialNotFoundError('Unable to find completion state named {}.'.format(value))
        return result

    @classmethod
    def from_name(cls, name):
        result = next((c for c in cls if c.name == name), None)
        if result is None:
            raise exc.MedialNotFoundError('Unable to find completion state named {}.'.format(name))
        return result


class GameState(object):

    def __init__(self, active: bool=False, **kwargs) -> None:
        self.active = active
        super().__init__()

    def as_dict(self):
        return dict(
            active=self.active,

        )


class GameState(GameState):

    def __init__(self, active=False, current_turn=None, board=None) -> None:
        super().__init__(active)


class Game(ABC):

    def __init__(self, key: str) -> None:
        self.key = key
        self.store = store.get_store(key)
        self.store.set(self.store.CONTEXT_TYPE, self.game_type())
        super().__init__()

    @classmethod
    def game_type(cls):
        # type: (None) -> str
        # returns a url-friendly string to represent this game type, e.g. 'connect-four'
        return inflection.dasherize(inflection.underscore(cls.__name__))

    @classmethod
    def validate_new_state(cls, current_state: dict, new_state: dict) -> CompletionState:
        raise NotImplementedError

    @property
    def state(self) -> GameState:
        raise NotImplementedError

    @state.setter
    def state(self, new_state: dict) -> None:
        raise NotImplementedError

    @property
    def clients(self) -> List[str]:
        raise NotImplementedError

    @clients.setter
    def clients(self, val: List[str]):
        raise NotImplementedError

    def register(self, client_name: str) -> None:
        raise NotImplementedError

    def start(self):
        raise NotImplementedError

    def delete(self):
        # completely removes this game from storage
        raise NotImplementedError