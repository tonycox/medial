import enum
import logging
import random
import os
from functools import wraps
import uuid

import requests
from flask import json, request, Response
from flask_cors import CORS

from medial import game, exc, store
from .application import App


logger = logging.getLogger(__name__)

app = App(__name__)
CORS(app)

INCOMPLETE_STUB = 'This requested URL has not yet been implemented on the server.'
INVALID_REQUEST = 'Invalid Request'

GET = 'GET'
PUT = 'PUT'
POST = 'POST'

RANDOM_KEY_LENGTH = 4


class Result(object):
    class Status(enum.Enum):
        SUCCESS = 'success'
        FAIL = 'fail'
        ERROR = 'error'

    STATUS = 'status'
    MESSAGE = 'message'
    DATA = 'data'
    ERROR_CODE = 'code'

    def __init__(self, status: Status, message: str = None, data: dict = None, http_status_code: int=None) -> None:
        self.status = status  # required for all results
        self.message = message  # required for error results
        self.data = data  # required for success/fail results
        self.http_status_code = http_status_code
        super().__init__()

    @property
    def is_error(self):
        return self.status == self.Status.ERROR

    @classmethod
    def from_exception(cls, e: Exception) -> 'Result':
        if not isinstance(e, exc.MedialException):
            message = 'Unexpected Server Error'
            status = cls.Status.ERROR
        else:
            message = 'There is a problem with your request'
            status = cls.Status.FAIL
        return cls(
            status=status,
            message='{}. Original Error: {}'.format(message, e)
        )

    def as_dict(self) -> dict:
        return {
            self.STATUS: self.status.value,
            self.MESSAGE: self.message,
            self.DATA: self.data,
        }

    def jsonified(self):
        response = self.as_dict()
        http_response = json.jsonify(response)
        http_response.status_code = self.http_status_code or {
            self.Status.SUCCESS: 200,
            self.Status.FAIL: 400,
            self.Status.ERROR: 500
        }.get(self.status)
        return http_response


def _assert_game_is_correct_type(game, game_type_name):
    if game.game_type() != game_type_name:
        raise exc.MedialAssertionError('Game with key {} has type {}, expected {}'.format(
            game.key, game.game_type(), game_type_name
        ))


def requires_api_key(func):
    @wraps(func)
    def decorated_function(*args, **kwargs):
        if os.getenv('REQUIRE_API_KEY', 'False') == 'True':
            raw_keys = store.get_store('api').get('keys')
            if not raw_keys:
                return Result(
                    Result.Status.ERROR,
                    message='There are no API keys stored on the server, yet API keys are required by the server',
                ).jsonified()
            api_keys = json.loads(raw_keys)
            provided_key = request.headers.get('x-api-key')
            if provided_key is None:
                return Result(
                    Result.Status.FAIL,
                    message='No API key provided. Please include a valid API key in request header x-api-key',
                    http_status_code=401,
                ).jsonified()
            for name, key in api_keys.items():
                if provided_key == key:
                    return func(*args, **kwargs)
            return Result(
                Result.Status.FAIL,
                message='Incorrect API key provided. Please include a valid API key in request header x-api-key',
                http_status_code=401,
            ).jsonified()
        else:
            return func(*args, **kwargs)
    return decorated_function


@app.route('/add_api_key', methods=[POST])
@requires_api_key
def add_api_key():
    try:
        user_name = request.args.get('user')
        if not user_name:
            raise exc.MedialAssertionError('No user provided in URL query string')
        raw_keys = store.get_store('api').get('keys')
        api_keys = json.loads(raw_keys) if raw_keys else {}
        new_key = ''.join(str(uuid.uuid4()).split('-')).upper()
        api_keys[user_name] = new_key
        store.get_store('api').set('keys', json.dumps(api_keys))
        return Result(Result.Status.SUCCESS, data={
            'api_key': new_key
        }).jsonified()
    except Exception as e:
        logger.exception(e)
        return Result.from_exception(e).jsonified()


@app.route('/<string:game_type_name>/state', methods=[GET, POST])
@requires_api_key
def state(game_type_name: str) -> Response:
    try:
        key = request.args.get('key')
        if key is None:
            raise exc.MedialAssertionError('No key provided in query')
        c = game.get_game(app, key)
        _assert_game_is_correct_type(c, game_type_name)
        current_state = c.state
        if not current_state:
            return Result(Result.Status.FAIL, message='No state found for {}'.format(key)).jsonified()
        elif request.method == GET:
            return Result(Result.Status.SUCCESS, data={
                'key': c.key,
                'state': c.state.as_dict()
            }).jsonified()
        elif request.method == POST:
            if request.data:
                data = json.loads(request.data)
            elif request.form:
                data = request.form
            else:
                return Result(Result.Status.FAIL, message='No new state data provided').jsonified()
            c.state = data['state']
            return Result(Result.Status.SUCCESS, data={
                'key': c.key,
                'state': c.state.as_dict(),
            }).jsonified()
        else:
            raise exc.MedialAssertionError('Unsupported request method {}'.format(request.method))
    except Exception as e:
        logger.exception(e)
        return Result.from_exception(e).jsonified()


@app.route('/<string:game_type_name>/new', methods=[POST])
@requires_api_key
def new(game_type_name: str) -> Response:
    try:
        key = request.args.get('key')
        while key is None or game.game_exists(key):
            word_site = "http://svnweb.freebsd.org/csrg/share/dict/words?view=co&content-type=text/plain"
            response = requests.get(word_site)
            words = response.content.splitlines()
            first_word = random.choice(words).decode('utf-8').lower()
            second_word = random.choice(words).decode('utf-8').lower()
            key = '{}-{}'.format(first_word, second_word)  # relatively high entropy at about 3x10^8 combinations

        c = game.create_new_game(app, game_type_name, key)
        _assert_game_is_correct_type(c, game_type_name)
        return Result(Result.Status.SUCCESS, data={
            'key': key,
            'state': c.state.as_dict()
        }).jsonified()
    except Exception as e:
        logger.exception(e)
        return Result.from_exception(e).jsonified()


@app.route('/<string:game_type_name>/register', methods=[POST])
@requires_api_key
def register(game_type_name: str) -> Response:
    try:
        key = request.args.get('key')
        c = game.get_game(app, key)
        _assert_game_is_correct_type(c, game_type_name)
        if request.data:
            data = json.loads(request.data)
        elif request.form:
            data = request.form
        else:
            return Result(Result.Status.FAIL, message='No client data provided in post request').jsonified()
        client_name = data.get('clientName')
        if client_name is None:
            return Result(
                Result.Status.FAIL, message='Post data is missing required element "clientName"'
            ).jsonified()
        c.register(client_name)
        return Result(Result.Status.SUCCESS, data={
            'key': c.key,
            'state': c.state.as_dict(),
        }).jsonified()
    except Exception as e:
        logger.exception(e)
        return Result.from_exception(e).jsonified()


@app.route('/<string:game_type_name>/delete', methods=[POST])
@requires_api_key
def delete(game_type_name: str) -> Response:
    try:
        key = request.args.get('key')
        c = game.get_game(app, key)
        _assert_game_is_correct_type(c, game_type_name)
        c.delete()
        return Result(Result.Status.SUCCESS, data={
            'key': None,
            'state': None
        }).jsonified()
    except Exception as e:
        logger.exception(e)
        return Result.from_exception(e).jsonified()
