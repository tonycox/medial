from flask import Flask

from medial.game import Game
from medial import exc

from typing import Dict, Type


class App(Flask):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.game_types = {}  # type: Dict[str, Type[Game]]

    def add_game_type(self, game_type: Type[Game]):
        name = game_type.game_type()
        if self.game_types.get(name) is not None:
            raise exc.MedialKeyError('Game type {} has already been added to application'.format(name))
        self.game_types[name] = game_type
