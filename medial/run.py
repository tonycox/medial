import logging

from medial.api.routes import app
from connect_four import game


def run_basic_server():
    logging.basicConfig(format='%(asctime)s | %(levelname)s\t| %(name)s |  %(message)s', level=logging.DEBUG)
    logger = logging.getLogger(__name__)
    logger.info("Running API")
    app.add_game_type(game.ConnectFour)
    app.run()


if __name__ == '__main__':
    run_basic_server()