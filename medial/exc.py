class MedialException(Exception):
    pass


class MedialKeyError(MedialException, KeyError):
    pass


class MedialAssertionError(MedialException, AssertionError):
    pass


class MedialTypeError(MedialException, TypeError):
    pass


class MedialNotFoundError(MedialException):
    pass


class MedialValidationError(MedialException):
    pass