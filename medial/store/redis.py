import os
import redis
from flask import g

from typing import List

from . import base


class RedisStore(base.Store):
    KEY_JOIN = '__'

    def __init__(self, game_key: str) -> None:
        self.game_key = game_key
        if getattr(g, 'redis_cache', None) is None:
            # we store redis_cache on the global g object so we don't have to keep connecting to it.
            # this also allows us to set a test cache for testing purposes
            g.redis_cache = redis.StrictRedis(
                host=os.getenv('REDIS_HOST', 'localhost'),
                port=os.getenv('REDIS_PORT', 6379),
                password=os.getenv('REDIS_PASSWORD', None),
                db=os.getenv('REDIS_DB_NUMBER', 0),
                decode_responses=True
            )
        self._cache = g.redis_cache
        super().__init__()

    def gameualise_key(self, value_key: str):
        if self.KEY_JOIN in value_key:
            raise base.StoreException('Illegal string in key name: "{}"'.format(self.KEY_JOIN))
        return '{}{}{}'.format(self.game_key, self.KEY_JOIN, value_key)

    def set(self, value_key, val):
        return self._cache.set(self.gameualise_key(value_key), val)

    def get(self, value_key):
        return self._cache.get(self.gameualise_key(value_key))

    def delete(self, value_key):
        return self._cache.delete(self.gameualise_key(value_key))