from medial import exc

class Store(object):
    CONTEXT_TYPE = 'game_type'

    # def __init__(self, game_key: str) -> None:
    #     super().__init__()
    #
    # @classmethod
    # def combine_key(cls, game_key: str, value_key: str):
    #     if cls.KEY_JOIN in game_key or cls.KEY_JOIN in value_key:
    #         raise _base.StoreException('Illegal string in key: "{}"'.format(cls.KEY_JOIN))
    #     return '{}{}{}'.format(game_key, cls.KEY_JOIN, value_key)
    #
    # def set(self, game_key, value_key, val):
    #     raise NotImplementedError
    #
    # def get(self, game_key, value_key):
    #     raise NotImplementedError
    #
    # def delete(self, game_key, value_key):
    #     raise NotImplementedError


class StoreException(exc.MedialException):
    pass
