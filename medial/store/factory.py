import os
from .redis import RedisStore

_STORE_TYPES = {
    'redis': RedisStore
}


def get_store(game_key):
    store_type=os.getenv('STORE_TYPE', None)
    return _STORE_TYPES.get(store_type, RedisStore)(game_key)