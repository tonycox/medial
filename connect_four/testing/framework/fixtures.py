import json

import pytest
import redis
from flask import g, testing
from werkzeug.datastructures import Headers

from medial.api import routes
from connect_four import game

TEST_DEVELOPER = 'test-developer'
TEST_API_KEY = 'TEST-API-KEY'
API_HEADER_IDENTIFIER = 'x-api-key'


class TestClient(testing.FlaskClient):
    def open(self, *args, **kwargs):
        api_key_headers = Headers({
            API_HEADER_IDENTIFIER: TEST_API_KEY
        })
        headers = kwargs.pop('headers', Headers())
        headers.extend(api_key_headers)
        kwargs['headers'] = headers
        return super().open(*args, **kwargs)


@pytest.fixture(scope='session')
def app():
    with routes.app.app_context():
        routes.app.add_game_type(game.ConnectFour)
        yield routes.app


@pytest.fixture(scope='session')
def test_client(app):
    app.test_client_class = TestClient
    return app.test_client()


@pytest.fixture(scope='function', autouse=True)
def redis_cache(app):
    g.redis_cache = redis.StrictRedis(db=15, decode_responses=True)
    g.redis_cache.flushall()
    g.redis_cache.set('api__keys', json.dumps({
        TEST_DEVELOPER: TEST_API_KEY
    }))
    yield g.redis_cache
    g.redis_cache.flushall()