import redis
from flask import g

from medial.api import routes


def test_that_session_scoped_fixtures_work(app, test_client):
    assert app == routes.app
    assert test_client.application == app
    assert type(g.redis_cache) == redis.StrictRedis
    assert g.redis_cache.connection_pool.connection_kwargs['db'] == 15
    assert g.redis_cache.connection_pool.connection_kwargs['decode_responses']
