import json

import redis
from flask import Response
from flask.testing import FlaskClient

from medial.api import routes
from connect_four import game

CONTEXT_TYPE = 'connect-four'
KEY = '1234'
FIRST_PLAYER = 'Suzie'
SECOND_PLAYER = 'Jenny'


def _response_as_string(response, message: str=''):
    # assumes json.loads returns a dict
    if response.data is not None:
        return message + '\n' + '\n'.join('{}: {}'.format(k, v) for k, v in json.loads(response.data).items())
    return message + str(response)  # just rely on the Response __str__ implementation


def _assert_response_is_error(response: Response):
    assert type(response) == Response
    assert response.status_code == 500, _response_as_string(response, 'incorrect error status code')
    response_data = json.loads(response.data)
    assert 'message' in response_data.keys(), _response_as_string(response, 'missing message')
    assert 'status' in response_data.keys(), _response_as_string(response, 'missing status')
    assert response_data.get('data') is None, _response_as_string(response, 'error has data')
    assert response_data['message'], _response_as_string(response, 'message is empty')
    assert 'Unknown Exception' not in response_data['message'], _response_as_string(
        response, 'error comes from outside medial'
    )
    assert response_data['status'] == routes.Result.Status.ERROR.value, _response_as_string(
        response, 'incorrect status'
    )


def _assert_response_is_fail(response: Response):
    assert type(response) == Response
    assert response.status_code == 400, _response_as_string(response, 'incorrect fail status code')
    response_data = json.loads(response.data)
    assert 'message' in response_data.keys(), _response_as_string(response, 'missing message')
    assert 'status' in response_data.keys(), 'missing status'
    assert response_data['message'], 'message is empty'
    assert response_data['status'] == routes.Result.Status.FAIL.value, 'incorrect status'


def _assert_response_is_success(response: Response):
    assert type(response) == Response
    assert response.status_code == 200, _response_as_string(response, 'incorrect success status code.')
    response_data = json.loads(response.data)
    assert 'data' in response_data.keys(), _response_as_string(response, 'missing data.')
    assert 'status' in response_data.keys(), _response_as_string(response, 'missing status.')
    assert 'message' in response_data.keys(), _response_as_string(response, 'missing message.')
    assert response_data['message'] is None
    assert response_data['data'], _response_as_string(response, 'data is empty')
    assert response_data['status'] == routes.Result.Status.SUCCESS.value, _response_as_string(
        response, 'incorrect status'
    )


def _get_data_from_response(response: Response):
    return json.loads(response.data)['data']


def _create_new_connect_four_instance(test_client: FlaskClient) -> Response:
    response = test_client.post('{}/new?key={}'.format(CONTEXT_TYPE, KEY))
    _assert_response_is_success(response)
    return response


def _delete_connect_four_instance(test_client: FlaskClient) -> Response:
    response = test_client.post('{}/delete?key={}'.format(CONTEXT_TYPE, KEY))
    _assert_response_is_success(response)
    return response


def _register_client(test_client: FlaskClient, client_name: str):
    response = test_client.post(
        '{}/register?key={}'.format(CONTEXT_TYPE, KEY),
        data=json.dumps(dict(clientName=client_name, foo='bar')),
    )
    _assert_response_is_success(response)
    return response


def _get_state(test_client: FlaskClient):
    response = test_client.get('{}/state?key={}'.format(CONTEXT_TYPE, KEY))
    _assert_response_is_success(response)
    return response


def test_valid_requests_for_nonexistent_game_fail(test_client: FlaskClient) -> None:
    response = test_client.get('/{}/state?key={}/'.format(CONTEXT_TYPE, KEY))  # type: Response
    _assert_response_is_fail(response)
    response = test_client.post('/{}/register?key={}/'.format(CONTEXT_TYPE, KEY))
    _assert_response_is_fail(response)
    response = test_client.post('/{}/delete?key={}'.format(CONTEXT_TYPE, KEY))
    _assert_response_is_fail(response)


def test_disallowed_methods_give_404_code(test_client: FlaskClient) -> None:
    urls = {'state', 'new', 'register', 'delete'}
    get_urls = {'state'}
    post_urls = {'new', 'register', 'delete'}
    put_urls = set()
    delete_urls = set()
    for url in urls.difference(get_urls):
        response = test_client.get(url)
        assert response.status_code == 404, 'Url {} should not be accessable via get request'.format(url)
    for url in urls.difference(post_urls):
        response = test_client.post(url)
        assert response.status_code == 404, 'Url {} should not be accessable via post request'.format(url)
    for url in urls.difference(put_urls):
        response = test_client.put(url)
        assert response.status_code == 404, 'Url {} should not be accessable via put request'.format(url)
    for url in urls.difference(delete_urls):
        response = test_client.put(url)
        assert response.status_code == 404, 'Url {} should not be accessable via delete request'.format(url)


def test_can_create_new_game_and_delete_it(test_client: FlaskClient, redis_cache: redis.StrictRedis) -> None:
    _create_new_connect_four_instance(test_client)
    key = next((key for key in redis_cache.keys() if KEY in key), None)
    assert key is not None, 'Game did not create any keys in store'
    _delete_connect_four_instance(test_client)
    assert next((key for key in redis_cache.keys() if KEY in key), None) is None, 'Game not deleted successfully'


def test_register_new_client(test_client: FlaskClient):
    _create_new_connect_four_instance(test_client)
    _register_client(test_client, FIRST_PLAYER)
    response = _get_state(test_client)
    state_dict = _get_data_from_response(response)['state']
    assert state_dict['active'] is False, _response_as_string(response, 'State should not be active')
    players = state_dict['clients']
    assert players, _response_as_string(response, 'Client not successfully registered')
    assert len(players) == 1, _response_as_string(response, 'More than one client is registered')
    first_player = next(iter(players))
    assert first_player == FIRST_PLAYER
    colours = state_dict['client_colours']
    assert colours, _response_as_string(response, 'No client colours have been created')
    assert isinstance(colours, dict), _response_as_string(response, 'Colours is not a dict')
    player_colour = colours.get(first_player)
    assert player_colour, _response_as_string(response, 'No colour assigned to client')
    assert isinstance(player_colour, str)


def test_register_two_clients(test_client: FlaskClient):
    _create_new_connect_four_instance(test_client)
    _register_client(test_client, FIRST_PLAYER)
    _register_client(test_client, SECOND_PLAYER)
    response = _get_state(test_client)
    state_dict = _get_data_from_response(response)['state']
    assert state_dict['active'] is False, _response_as_string(response, 'State should not be active')
    players = state_dict['clients']
    assert len(players) == 2, _response_as_string(response, 'Unexpected number of clients registered')
    first_player, second_player = tuple(players)
    assert first_player == FIRST_PLAYER
    assert second_player == SECOND_PLAYER
    colours = state_dict['client_colours']
    first_player_colour = colours.get(first_player)
    second_player_colour = colours.get(second_player)
    assert first_player_colour != second_player_colour


def test_register_three_clients_fails(test_client: FlaskClient):
    _create_new_connect_four_instance(test_client)
    _register_client(test_client, FIRST_PLAYER)
    _register_client(test_client, SECOND_PLAYER)
    response = test_client.post(
        '{}/register?key={}'.format(CONTEXT_TYPE, KEY),
        data=json.dumps(dict(clientName='Player Threee')),
    )
    _assert_response_is_fail(response)


def test_start_game_fails_with_not_enough_players(test_client: FlaskClient):
    _create_new_connect_four_instance(test_client)
    _register_client(test_client, FIRST_PLAYER)
    response = _get_state(test_client)
    state_dict = _get_data_from_response(response)['state']
    state_dict['active'] = True
    response = test_client.post('{}/state?key={}'.format(CONTEXT_TYPE, KEY), data=json.dumps({'state': state_dict}))
    _assert_response_is_fail(response)


def _start_a_valid_two_player_game(test_client: FlaskClient):
    _create_new_connect_four_instance(test_client)
    _register_client(test_client, FIRST_PLAYER)
    _register_client(test_client, SECOND_PLAYER)
    response = _get_state(test_client)
    state_dict = _get_data_from_response(response)['state']
    state_dict['active'] = True
    return test_client.post('{}/state?key={}'.format(CONTEXT_TYPE, KEY), data=json.dumps({'state': state_dict}))


def test_start_game_succeeds_with_two_players_and_has_valid_initial_state(test_client: FlaskClient):
    response = _start_a_valid_two_player_game(test_client)
    _assert_response_is_success(response)
    state_dict = _get_data_from_response(response)['state']
    assert state_dict['active'] == True, _response_as_string(response, 'game is not active')
    assert state_dict.get('completion_state'), _response_as_string(response, 'game has no completion state')
    assert state_dict['completion_state'] == 'Incomplete', _response_as_string(
        response, 'state.completion_state should be Incomplete'
    )
    assert state_dict.get('turn_number') is not None, _response_as_string(response, 'game has no no turn number')
    assert state_dict['turn_number'] == 1, _response_as_string(response, 'turn number should be 1')
    assert state_dict.get('current_turn'), _response_as_string(response, 'no player has been assigned to turn 1')
    assert state_dict['current_turn'] in (FIRST_PLAYER, SECOND_PLAYER), _response_as_string(
        response, 'current turn not assigned to a registered player'
    )
    board = state_dict.get('board')
    assert board, _response_as_string(response, 'no board data returned')
    assert isinstance(board, list), _response_as_string(response, 'board state is not a dictionary')
    assert len(board) == 6, _response_as_string(response, 'connect-four board state should have exactly 6 keys')
    for row_num, row in enumerate(board):
        assert isinstance(row, list), _response_as_string(
            response, 'vals for row {} are not in list format'.format(row_num)
        )
        assert len(row) == 7, _response_as_string(
            response, 'number of vals for row {} does not equal 7'.format(row_num)
        )
        for col_num, cell in enumerate(row):
            assert cell is None, _response_as_string(
                response, 'row {}, column {} has initial state not set to None'.format(row_num, col_num)
            )


def _make_valid_turn(test_client: FlaskClient, column: int=None):
    state = _get_data_from_response(_get_state(test_client))['state']
    assert state.get('board') is not None, 'board has not been initialised, cannot make a valid turn'
    board = state['board']
    colour = state['client_colours'][state['current_turn']]
    for row_num in range(game.NUM_ROWS):
        for col_num in range(game.NUM_COLS):
            if column is None or col_num == column:
                if board[row_num][col_num] is None:
                    board[row_num][col_num] = colour
                    response = test_client.post(
                        '{}/state?key={}'.format(CONTEXT_TYPE, KEY), data=json.dumps({'state': state})
                    )
                    return response
    raise AssertionError('Unable to make a valid turn. {} is full!'.format(
        'Column {}'.format(column) if column is not None else 'Board'
    ))


def test_valid_first_turn(test_client: FlaskClient):
    state = _get_data_from_response(
        _start_a_valid_two_player_game(test_client)
    )['state']
    colour = state['client_colours'][state['current_turn']]
    response = _make_valid_turn(test_client)
    _assert_response_is_success(response)
    state = _get_data_from_response(response)['state']
    assert state['board'][0][0] == colour, 'Board not in expected state'


def test_found_victory(test_client: FlaskClient):
    response = _start_a_valid_two_player_game(test_client)
    state = _get_data_from_response(response)['state']
    starting_player = state['current_turn']
    _make_valid_turn(test_client, 0)
    _make_valid_turn(test_client, 1)
    _make_valid_turn(test_client, 0)
    _make_valid_turn(test_client, 1)
    _make_valid_turn(test_client, 0)
    _make_valid_turn(test_client, 1)
    response = _make_valid_turn(test_client, 0)
    _assert_response_is_success(response)
    state = _get_data_from_response(response)['state']
    assert state['completion_state'] == 'Complete', 'Game not completed with valid winning move'
    assert state.get('winner') is not None, 'Winner not set after valid winning move'
    assert state['winner'] == starting_player, 'Wrong player assigned victory'
    assert state.get('active') is False, 'Game not set to inactive after valid winning move'
    assert state['current_turn'] is None, 'Game has current turn set after valid winning move'


def test_found_stalemate(test_client: FlaskClient):
    _start_a_valid_two_player_game(test_client)
    stalemate_column_order = (0, 1, 2, 4, 3, 5)  # if columsn fill in this order it's a stalemate

    for col in stalemate_column_order:
        # place one piece in column 6, which offsets each column which is filled
        _make_valid_turn(test_client, 6)
        for _ in range(game.NUM_ROWS):
            try:
                state = _get_data_from_response(_make_valid_turn(test_client, col))['state']
                assert state.get('winner') is None, 'Winner set incorrectly'
            except Exception:
                raise
    state = _get_data_from_response(_get_state(test_client))['state']
    assert state['completion_state'] == 'Complete', 'Game not completed with full board (stalemate)'
    assert state.get('winner') is None, 'Winner set for stalemate'
    assert state.get('active') is False, 'Game still active with full board (stalemate)'
    assert state.get('current_turn') is None, 'Game has current turn set after stalemate'
    assert state.get('turn_number') == 42, 'Game has ended in stalemate without correct number of turns'


def test_state_post_request_with_no_change_fails(test_client: FlaskClient):
    response = _start_a_valid_two_player_game(test_client)
    for _ in range(6):
        response = _make_valid_turn(test_client, 0)
    # column 0 should be full now, try to make another turn in it
    state = _get_data_from_response(response)['state']
    response = test_client.post(
        '{}/state?key={}'.format(CONTEXT_TYPE, KEY), data=json.dumps({'state': state})
    )
    _assert_response_is_fail(response)


def test_invalid_turn_fails(test_client: FlaskClient):
    response = _start_a_valid_two_player_game(test_client)
    for _ in range(6):
        response = _make_valid_turn(test_client, 0)
    # column 0 should be full now, try to make another turn in it
    state = _get_data_from_response(response)['state']
    board = state['board']
    colour = state['client_colours'][state['current_turn']]
    board[1][0] = colour
    response = test_client.post(
        '{}/state?key={}'.format(CONTEXT_TYPE, KEY), data=json.dumps({'state': state})
    )
    _assert_response_is_fail(response)
