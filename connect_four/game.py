import enum
import collections as coll
import logging
import json
import random
from typing import List, Dict, Union, Tuple, Optional

from medial import exc
from medial.game import base


logger = logging.getLogger(__name__)
"""
example_state = {
    'game_type': 'ConnectFour',
    'clients': ['Joe', 'Judy'],
    'client_colours': {'Joe': 'Y', 'Judy': 'R'},
    'current_turn': 'Judy',
    'board': [
        ['Y', None, None, None, None, None, None],
        ['R', None, None, None, None, None, None],
        ['Y', None, None, None, None, None, None],
        ['R', 'Y', 'R', 'Y', 'R', 'Y', None],
        ['Y', None, None, None, None, None, None],
        ['R', None, None, None, None, None, None],
    ]
}
board[0] is the bottom (not top) row of the board
board[0][0] is the bottom left cell of the board
"""

NUM_ROWS = 6
NUM_COLS = 7
NUM_CONSECUTIVE_REQUIRED_FOR_VICTORY = 4


class Colour(enum.Enum):
    RED = 'R'
    YELLOW = 'Y'

    @classmethod
    def num_colours(cls):
        return len([c for c in cls])


class ConnectFourState(base.GameState):
    def __init__(
            self,
            active: bool,
            clients: List[str]=None,
            client_colours: Dict[str, Colour]=None,
            board: List[List[Optional[str]]]=None,
            completion_state: str=base.CompletionState.INCOMPLETE.value,
            current_turn: str=None,
            turn_number: int=None,
            game_type: str=None,
            winner: str=None,
            **kwargs
    ) -> None:
        super().__init__(active=active, **kwargs)
        self.game_type = ConnectFour.game_type()
        if game_type is not None and game_type != self.game_type:
            raise exc.MedialAssertionError('cannot set a game type that is not {}'.format(self.game_type))
        self.clients = clients or []
        self.client_colours = client_colours or {}
        self.completion_state = base.CompletionState.from_value(completion_state)
        self.board = board
        self.turn_number = turn_number
        self.winner = winner
        self.current_turn = current_turn
        if self.active:
            if not self.game_has_started():
                self._start_game()
            if self.current_turn not in self.clients:
                raise exc.MedialAssertionError('current_turn must be a registered client')

    def game_has_started(self):
        return (
            self.active
            and self.board is not None
            and self.current_turn is not None
            and self.turn_number is not None
        )

    def _start_game(self):
        # make sure we have two clients
        if len(self.clients) != len([c for c in Colour]):
            raise exc.MedialAssertionError('not enough players to start a game')
        self.board = self.get_blank_board()
        self.current_turn = self.clients[random.randint(0, 1)]
        self.turn_number = 1
        if not self.game_has_started():  # probably unnecessary, just check that this method does everything it should
            raise exc.MedialAssertionError('game not started successfully')

    @staticmethod
    def get_blank_board():
        return [[None for _ in range(7)] for _ in range(6)]

    def as_dict(self):
        d = super().as_dict()
        d.update(dict(
            game_type=self.game_type,
            clients=self.clients,
            client_colours=self.client_colours,
            completion_state=self.completion_state.value,
            board=self.board,
            winner=self.winner,
            current_turn=self.current_turn,
            turn_number=self.turn_number,
        ))
        return d

    def as_json_str(self) -> str:
        return json.dumps(self.as_dict())

    @classmethod
    def from_json_str(cls, json_str):
        try:
            return cls(**json.loads(json_str))
        except TypeError as e:
            logger.exception(e)
            raise exc.MedialTypeError(
                'Json string incompatible with ConnectFourState initialiser. Original Error: {}'.format(e)
            )

    def __eq__(self, o: 'ConnectFourState') -> bool:
        return (
            self.current_turn == o.current_turn
            and self.clients == o.clients
            and self.client_colours == o.client_colours
            and self.board == o.board
            and self.turn_number == o.turn_number
            and self.winner == o.winner
            and self.completion_state == o.completion_state
            and self.game_type == o.game_type
            and self.turn_number == o.turn_number
        )


class ConnectFour(base.Game):
    STATE = 'state'
    CLIENTS = 'clients'

    @property
    def clients(self) -> List[str]:
        return self.state.clients

    def validate_new_state(self, current_state: ConnectFourState, new_state: ConnectFourState) -> None:
        ConnectFourValidator(current_state, new_state, self).validate()

    @property
    def state(self) -> ConnectFourState:
        state_str = self.store.get(self.STATE)
        if state_str is None:
            state = ConnectFourState(active=False)
            self.store.set(self.STATE, state.as_json_str())
            state_str = self.store.get(self.STATE)
        return ConnectFourState.from_json_str(state_str)

    @state.setter
    def state(self, new_state: Union[dict, ConnectFourState]) -> None:
        if isinstance(new_state, dict):
            state_dict = self.state.as_dict()
            state_dict.update(new_state)  # any missing keys from new state are taken from current state
            new_state = ConnectFourState(**state_dict)
        self.validate_new_state(self.state, new_state)
        # if we get to here, validation was successful and we can store the new state
        self.store.set(self.STATE, new_state.as_json_str())

    def _get_next_colour(self):
        for colour in Colour:
            if colour.value not in self.state.client_colours.values():
                return colour
        raise exc.MedialNotFoundError('Unable to find an available colour. All colours have been taken')

    def register(self, client_name: str) -> None:
        if client_name in self.state.clients:
            raise exc.MedialAssertionError('Client {} is already registered'.format(client_name))
        state = self.state
        state.clients.append(client_name)
        available_colour = self._get_next_colour()
        state.client_colours[client_name] = available_colour.value
        self.state = state  # need to do this to write back to db

    def start(self):
        # build an initial state and set it.  Need to ensure it passes validation
        pass

    def delete(self):
        # completely removes this game from storage
        self.store.delete(self.store.CONTEXT_TYPE)
        self.store.delete(self.STATE)
        self.store.delete(self.CLIENTS)


class ConnectFourValidator(object):

    def __init__(
            self,
            current_state: ConnectFourState,
            new_state: ConnectFourState,
            game: ConnectFour,
            banned_attributes: List[str]=None
    ) -> None:
        self.current_state = current_state
        self.new_state = new_state
        self.game = game
        self.banned_attributes = banned_attributes or []
        super().__init__()

    def _validate_state_has_changed(self) -> None:
        if self.new_state == self.current_state:
            raise exc.MedialValidationError("New state is identical to current state")

    def validate(self) -> None:
        self._validate_state_has_changed()
        self._validate_client_change()
        self._validate_client_colour_change()
        self._validate_active_change()
        self._validate_game_type_change()
        self._validate_board_change()

    @classmethod
    def _get_board_cells(cls, board: List[List[Optional[str]]]) -> Dict[Tuple[int, int], Optional[str]]:
        """transforms
            {'1': ['Y', 'R', None, None]}
                into
            {(1, 1): 'Y', (1, 2): R...}
            This allows all cell values to be in cells.values() and be able to be indexed by (row, col) tuple
        """
        cells = {}
        for row_num, row in enumerate(board):
            for col_num, cell in enumerate(row):
                cells[(row_num, col_num)] = cell
        return cells

    def _validate_board_change(self) -> None:
        new_board = self.new_state.board
        current_board = self.current_state.board
        if new_board == current_board:
            return
        if new_board == ConnectFourState.get_blank_board() and current_board is None:
            return
        # total number of rows/cols must still be correct and match keys of previous
        if len(new_board) < NUM_ROWS:
            raise exc.MedialValidationError('Board must have {} rows. Found {}.'.format(
                NUM_ROWS, len(new_board)
            ))
        for i, row in enumerate(new_board):
            if len(row) < NUM_COLS:
                raise exc.MedialValidationError('Board must have {} cols for all rows. Found {} cols for row {}'.format(
                    NUM_COLS, len(row), i
                ))
        current_board_cells = self._get_board_cells(current_board)
        new_board_cells = self._get_board_cells(new_board)
        # all cells can only have values None, Y, R
        for cell_val in new_board_cells.values():
            if cell_val not in (None, Colour.YELLOW.value, Colour.RED.value):
                raise exc.MedialValidationError('All board cells must be either Y, R or None (null)')

        # total number of Nones must be exactly one less than before (as we have returned if equal)
        num_current_nones = len(tuple(c for c in current_board_cells.values() if c is None))
        num_new_nones = len(tuple(c for c in new_board_cells.values() if c is None))
        if num_new_nones != num_current_nones - 1:
            raise exc.MedialValidationError('Number of None (null) values must be one fewer than previous board')

        # get row/col and value of the cell which has changed and check that only one has changed
        def _cells_are_equal(old, new):
            if old is None and new is None:
                return True
            if old is None or new is None:
                return False
            if old == new:
                return True
            else:
                return False

        changed_cell = None
        for (new_row, new_col), new_val in new_board_cells.items():
            current_cell_val = current_board_cells[new_row, new_col]
            if not _cells_are_equal(current_cell_val, new_val):
                # this cell has changed
                if changed_cell is not None:
                    raise exc.MedialValidationError(
                        'More than one cell has changed.  Received new value at both ({},{}) and ({},{})'.format(
                            changed_cell.row, changed_cell.col, new_row, new_col
                        )
                    )
                else:
                    changed_cell = coll.namedtuple('Cell', 'row col value')(new_row, new_col, new_val)
        if changed_cell is None:
            raise exc.MedialValidationError('No cells have been changed')

        # check that changed cell has colour of player whose turn it was
        if self.current_state.current_turn is None:
            raise exc.MedialValidationError('Cannot determine whose turn it is.  Board change not permitted')
        current_turn_colour = self.current_state.client_colours[self.current_state.current_turn]
        if changed_cell.value != current_turn_colour:
            raise exc.MedialValidationError('Expected a new {} cell, instead received a new {} cell'.format(
                current_turn_colour, changed_cell.value
            ))

        # check that current turn has been updated, or else update it
        new_turn = next((c for c in self.current_state.clients if c != self.current_state.current_turn))
        if self.new_state.current_turn != new_turn:
            logger.info('Current turn not updated in new state.  Setting current turn to {}'.format(new_turn))
            self.new_state.current_turn = new_turn

        # search for victory
        self._search_for_and_process_victory(new_board_cells, changed_cell)

        # check for stalemate (no Nones left)
        if num_new_nones == 0 and self.new_state.winner is None:
            self.new_state.completion_state = base.CompletionState.COMPLETE
            self.new_state.current_turn = None
            self.new_state.active = False
            self.new_state.winner = None

        # increment turn number if required
        if self.new_state.turn_number == self.current_state.turn_number and self.new_state.active:
            self.new_state.turn_number += 1

    def _search_for_and_process_victory(self, new_board_cells, changed_cell):
        # todo: this can be made much, much cleaner.
        # See https://stackoverflow.com/questions/3311119/determining-three-in-a-row-in-python-2d-array

        #   check vertical
        #     # move down until we hit None or opposite colour:
        def process_victory():
            self.new_state.completion_state = base.CompletionState.COMPLETE
            self.new_state.current_turn = None
            self.new_state.active = False
            self.new_state.winner = self.current_state.current_turn

        num_consecutive = 1
        row_index = changed_cell.row - 1  # look below
        while row_index >= 0:
            if new_board_cells[row_index, changed_cell.col] != changed_cell.value:
                break
            num_consecutive += 1
            row_index -= 1
        row_index = changed_cell.row + 1  # look above
        while row_index < NUM_ROWS:
            if new_board_cells[row_index, changed_cell.col] != changed_cell.value:
                break
            num_consecutive += 1
            row_index += 1
        if num_consecutive >= NUM_CONSECUTIVE_REQUIRED_FOR_VICTORY:
            process_victory()
            return
        #   check all horizontal
        num_consecutive = 1
        column_index = changed_cell.col - 1  # look left
        while column_index >= 0:
            if new_board_cells[changed_cell.row, column_index] != changed_cell.value:
                break
            num_consecutive += 1
            column_index -= 1
        column_index = changed_cell.col + 1  # look right
        while column_index < NUM_COLS:
            if new_board_cells[changed_cell.row, column_index] != changed_cell.value:
                break
            num_consecutive += 1
            column_index += 1
        if num_consecutive >= NUM_CONSECUTIVE_REQUIRED_FOR_VICTORY:
            process_victory()
            return
        #    check all diagonals
        #      check positive diagonal (upwards from left to right)
        num_consecutive = 1
        row_index = changed_cell.row - 1  # look below
        column_index = changed_cell.col - 1  # look left
        while row_index >= 0 and column_index >= 0:
            if new_board_cells[row_index, column_index] != changed_cell.value:
                break
            num_consecutive += 1
            row_index -= 1
            column_index -= 1
        row_index = changed_cell.row + 1  # look above
        column_index = changed_cell.col + 1  # look right
        while row_index < NUM_ROWS and column_index < NUM_COLS:
            if new_board_cells[row_index, column_index] != changed_cell.value:
                break
            num_consecutive += 1
            row_index += 1
            column_index += 1
        if num_consecutive >= NUM_CONSECUTIVE_REQUIRED_FOR_VICTORY:
            process_victory()
            return
        #      check negative diagonal (downwards from left to right)
        num_consecutive = 1
        row_index = changed_cell.row - 1  # look below
        column_index = changed_cell.col + 1  # look right
        while row_index >= 0 and column_index < NUM_COLS:
            if new_board_cells[row_index, column_index] != changed_cell.value:
                break
            num_consecutive += 1
            row_index -= 1
            column_index += 1
        row_index = changed_cell.row + 1  # look above
        column_index = changed_cell.col - 1  # look left
        while row_index < NUM_ROWS and column_index >= 0:
            if new_board_cells[row_index, column_index] != changed_cell.value:
                break
            num_consecutive += 1
            row_index += 1
            column_index -= 1
        if num_consecutive >= NUM_CONSECUTIVE_REQUIRED_FOR_VICTORY:
            process_victory()
            return

    def _validate_client_change(self) -> None:
        if self.current_state.clients == self.new_state.clients:
            return  # list and string equality is a thing
        if len(self.new_state.clients) < len(self.current_state.clients):
            raise exc.MedialValidationError('Reducing registered clients manually is not allowed')
        if len(set(self.new_state.clients)) < len(self.new_state.clients):
            raise exc.MedialValidationError('Client names must be unique')
        for client in self.new_state.clients:
            if self.new_state.client_colours.get(client) is None:
                raise exc.MedialValidationError('Client {} has been registered without a colour'.format(client))

    def _validate_client_colour_change(self) -> None:
        if self.current_state.client_colours == self.new_state.client_colours:
            return  # equality check should be fine here, as it's a Dict[str, Enum]
        num_new_colours = len(self.new_state.client_colours.keys())
        num_current_colours = len(self.current_state.client_colours.keys())
        if num_new_colours < num_current_colours:
            raise exc.MedialValidationError('Reducing client colours is not allowed')
        elif num_new_colours == num_current_colours:
            for player, colour in self.new_state.client_colours.items():
                if self.current_state.client_colours.get('player') != colour:
                    raise exc.MedialValidationError('Changing client colours is not allowed')
        else:
            taken_colours = []
            for player, colour in self.new_state.client_colours.items():
                if colour in taken_colours:
                    raise exc.MedialValidationError('Multiple players are assigned to colour {}'.format(colour))
                taken_colours.append(colour)

    def _validate_active_change(self) -> None:
        if self.current_state.active == self.new_state.active:
            return
        if self.current_state.active:
            if not self.new_state.active:
                raise exc.MedialValidationError('Manual state change to inactive not allowed')
        else:
            if self.new_state.active and len(self.new_state.clients) != Colour.num_colours():
                raise exc.MedialValidationError('Not enough players have registerd to set game to active')

    def _validate_game_type_change(self) -> None:
        if self.current_state.game_type != self.new_state.game_type:
            raise exc.MedialValidationError('Changing game is not allowed')

    def _validate_completion_state_change(self) -> None:
        if self.new_state.completion_state != self.current_state.completion_state:
            raise exc.MedialValidationError('Manually changing completion state is not allowed')
