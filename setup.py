from setuptools import setup, find_packages

requires = [
    'flask',
    'flask-cors',
    'requests',
    'inflection',
]

test_deps = [
    'pytest',
    'redis'
]

redis_deps = [
    'redis'
]

deploy_deps = [
    'zappa'
]

doc_deps = [
    'flask-apispec'
]

extras = {
    'test': test_deps,
    'redis': redis_deps,
    'deploy': deploy_deps,
    'docs': doc_deps
}

setup(
    name='medial',
    version="1.0.0",
    description='A simple game mediator built into a web api',
    classifiers=[
        "Programming Language :: Python",
    ],
    author='Tony Cox',
    author_email='coxy.t82@gmail.com',
    url='',
    keywords='medial mediator web api',
    packages=find_packages(),
    include_package_data=True,
    zip_safe=False,
    install_requires=requires,
    tests_require=test_deps,
    extras_require=extras,
)
